SELECT customerName FROM customers
WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers
WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products
WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName
FROM customers
WHERE state IS NULL;

SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson"
AND
firstName = "Steve";

SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA" AND
creditLimit > 3000;

SELECT customerName
FROM customers
WHERE customerName
NOT LIKE "%a%";

SELECT customerNumber
FROM orders
WHERE comments
LIKE "%dhl%";

SELECT productLine
FROM products
WHERE productDescription
LIKE "%state of the art%";

SELECT DISTINCT country
FROM customers;

SELECT DISTINCT status
FROM orders;

SELECT customerName, country
FROM customers
WHERE country
IN ("USA", "France", "Canada");

SELECT firstName, lastName, offices.city
FROM employees
JOIN offices
ON employees.officeCode = offices.officeCode
WHERE employees.officeCode = 5;

SELECT customerName FROM customers
WHERE salesRepEmployeeNumber = 1166;

SELECT productName, customerName
FROM customers
JOIN orders
ON customers.customerNumber = orders.customerNumber
JOIN orderdetails
ON orders.orderNumber = orderdetails.orderNumber
JOIN products
ON orderdetails.productCode = products.productCode
WHERE orders.customerNumber = 121;

SELECT firstName, lastName, customers.customerName,
offices.country
FROM employees
JOIN customers
ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices
ON customers.country = offices.country
WHERE customers.country = offices.country;

SELECT lastName, firstName
FROM employees
WHERE reportsTo = 1143;

SELECT productName, MAX(MSRP)
FROM products;

SELECT COUNT(customerNumber)
FROM customers
WHERE country = "UK";

SELECT productLine, COUNT(productCode) AS number_of_products
FROM products
GROUP BY productLine;

SELECT employeeNumber,
employees.firstName,
employees.lastName,
COUNT(customers.customerNumber) AS customers_served
FROM customers
JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
GROUP BY salesRepEmployeeNumber;

SELECT productName, quantityInStock
FROM products
WHERE productLine = "Planes"
AND
quantityInStock < 1000;